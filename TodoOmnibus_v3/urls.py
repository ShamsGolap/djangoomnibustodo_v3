from django.conf.urls import patterns, include, url
from django.contrib import admin

from todo import views


admin.autodiscover()

urlpatterns = patterns(
    '',
    # url(r'^$', views.index, name='index'),
    url(r'^.*$', views.index, name='index'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
)
