/*globals WebSocket, SockJS, Omnibus, $, console */

(function () {
    'use strict';

    var // Attributes
        todos = {},
        transport = WebSocket,
        endpoint = '{{ OMNIBUS_ENDPOINT }}',
        //endpoint = 'ws://127.0.0.1:4242/ec',
        options = {
            authToken: '{{ OMNIBUS_AUTH_TOKEN }}',
            autoReconnect: true,
            autoReconnectTimeout: 1000
        },
        connection = new Omnibus(transport, endpoint, options),
        channel = connection.openChannel('mousemoves');

    connection.
        on(Omnibus.events.CONNECTION_CONNECTED, function (event) {
            //console.log(event);
            $('.connection').addClass('open').text('Yes');
        })
        .on(Omnibus.events.CONNECTION_AUTHENTICATED, function (event) {
            //console.log(event);
            $('.identification').addClass('open').text('Yes');
        });

    channel
        .on(Omnibus.events.CHANNEL_SUBSCRIBED, function (event) {
            //console.log(event);
            $('.channel').addClass('open').text('Yes');
        })
        .on('move', function (event) {
            var
                todo = todos[event.data.sender];

            if (todo === undefined) {
                todo = $('<div class="player" />').appendTo($('body'));
                todos[event.data.sender] = todo;
            }

            todo.css({
                top: event.data.payload.top,
                left: event.data.payload.left
            });
        })
        .on('disconnect', function (event) {
            var
                todo = todos[event.data.sender];

            if (todo !== undefined) {
                todo.remove();
                todos[event.data.sender] = undefined;
                delete (todos[event.data.sender]);
            }
        });

    $('body').mousemove(function (event) {
        channel.send('move', {top: event.pageY, left: event.pageX});
    });
}());
