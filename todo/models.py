# -*- coding: utf-8 -*-

from django.db import models


class Todo(models.Model):
    label = models.CharField(max_length=200)
    done = models.BooleanField(default=False)

    def __str__(self):
        if self.done:
            return u'\" ' + self.label + u' \" IS done'
        else:
            return u'\" ' + self.label + u' \" IS NOT done'

    def __unicode__(self):
        if self.done:
            return u'\" ' + self.label + u' \" IS done'
        else:
            return u'\" ' + self.label + u' \" IS NOT done'
